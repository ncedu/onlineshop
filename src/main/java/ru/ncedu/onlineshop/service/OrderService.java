package ru.ncedu.onlineshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.ncedu.onlineshop.entity.order.Order;
import ru.ncedu.onlineshop.entity.order.OrderItem;
import ru.ncedu.onlineshop.entity.product.Manufacturer;
import ru.ncedu.onlineshop.entity.product.Product;
import ru.ncedu.onlineshop.entity.users.User;
import ru.ncedu.onlineshop.exception.IncorrectStateException;
import ru.ncedu.onlineshop.service.dao.orders.OrderDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 31.12.14.
 */

@Component
public class OrderService {

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private Order currentOrder;

//    @Transactional
//    public Order getOrderWithItems(Order order){
//        return orderDAO.getOrderWithItems(order);
//    }

    @Transactional
    public List<Order> getOrders(){
        return orderDAO.findAll();
    }

//    @Transactional
//    public Order addOrder(Order order){
//        return orderDAO.save(order);
//    }

    @Transactional
    public void removeOrder(Order order){
        orderDAO.remove(order.getId());
    }

    @Transactional
    public List<Order> getOrdersOfUser(User user){
        return orderDAO.getOrdersOfUser(user);
    }

//    @Transactional
//    public Order getLastUserOrder(User user){
//        return orderDAO.getLastUserOrder(user);
//    }
//
//    @Transactional
//    public Order clearOrderItems(Order order){
//        return orderDAO.clearOrderItems(order);
//    }
//
//    @Transactional
//    public Order addOrderItem(Order order, OrderItem orderItem){
//        return orderDAO.addOrderItem(order, orderItem);
//    }

    @Transactional(rollbackFor = IncorrectStateException.class)
    public Order confirmOrder(Order order) throws IncorrectStateException {
        order.confirm();
        Order res = orderDAO.save(order);
        return  res;
    }

    @Transactional(rollbackFor = IncorrectStateException.class)
    public Order deliverOrder(Order order) throws IncorrectStateException {
        order.deliver();
        return orderDAO.save(order);
    }

    @Transactional(rollbackFor = IncorrectStateException.class)
    public Order completeOrder(Order order) throws IncorrectStateException {
        order.complete();
        return orderDAO.save(order);
    }

    @Transactional(rollbackFor = IncorrectStateException.class)
    public Order cancelOrder(Order order) throws IncorrectStateException {
        order.cancel();
        return orderDAO.save(order);
    }

    public Order getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(Order order) {

    }
}
