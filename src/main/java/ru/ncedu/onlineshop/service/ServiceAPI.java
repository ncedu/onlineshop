package ru.ncedu.onlineshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.ncedu.onlineshop.entity.order.Order;
import ru.ncedu.onlineshop.entity.product.*;
import ru.ncedu.onlineshop.service.dao.products.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Али on 09.09.14.
 */
@Component
@Transactional(readOnly = true)
public class ServiceAPI {

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ParameterDao parameterDao;

    @Autowired
    private ProductFieldDAO productFieldDAO;

    @Autowired
    private ProductTypeDAO productTypeDAO;

    @Autowired
    private ManufacturerDAO manufacturerDAO;

    private boolean isDatabaseInitialized = false;

    @Transactional
    public void initializeTestDB(){
        if (isDatabaseInitialized == true)
            return;
        isDatabaseInitialized = true;


        try {
            ProductType type = new ProductType("Shoe");
            ProductType subType = new ProductType("Usual shoe");
            type.getChildTypes().add(subType);
            subType.setParentType(type);
            type = addProductType(type);
            subType = type.getChildTypes().iterator().next();
            ProductType jeansType = new ProductType("Jeans");
            jeansType = addProductType(jeansType);
            List<ProductType> findType = getProductTypeTree();


            Manufacturer manufacturer = new Manufacturer("RalfRinger", "Motherland");
            manufacturer = addManufacturer(manufacturer);

            ProductField shoeSize = new ProductField("Shoe size", type);
            ProductField shoeMaterial = new ProductField("Shoe material", type);
            shoeSize =  addProductField(shoeSize);
            shoeMaterial = addProductField(shoeMaterial);

            ProductSize size = new ProductSize(10, 10, 40);
            List<Parameter> shoeParameters = new ArrayList<Parameter>();
            shoeParameters.add(new Parameter(shoeSize, "42"));
            shoeParameters.add(new Parameter(shoeMaterial, "leather"));
            Product product = new Product(subType, manufacturer, size, shoeParameters, 10000, "Usual man shoe", 2340);
            for (Parameter parameter: shoeParameters)
                parameter.setProduct(product);
            product = addProduct(product);



            size = new ProductSize(10, 10, 40);
            shoeParameters = new ArrayList<Parameter>();
            shoeParameters.add(new Parameter(shoeSize, "44"));
            shoeParameters.add(new Parameter(shoeMaterial, "natural leather"));
            Product product2 = new Product(type, manufacturer, size, shoeParameters, 10000, "Good woman shoe", 2340);
            for (Parameter parameter: shoeParameters)
                parameter.setProduct(product2);
            product2 = addProduct(product2);

            // jeans
            ProductField jeansSize = new ProductField("Jeans size", type);
            ProductField jeansMaterial = new ProductField("Jeans material", type);
            jeansSize = addProductField(jeansSize);
            jeansMaterial = addProductField(jeansMaterial);
            //ProductField
            ProductSize jeansProdSize = new ProductSize(50, 1, 120);
            List<Parameter> jeansParameters = new ArrayList<Parameter>();
            jeansParameters.add(new Parameter(jeansSize, "42"));
            jeansParameters.add(new Parameter(jeansMaterial, "Cotton"));
            Manufacturer jeansManufacturer = new Manufacturer("Huigun Zeipin", "China");
            jeansManufacturer = addManufacturer(jeansManufacturer);
            Product jeans = new Product(jeansType, jeansManufacturer, jeansProdSize, jeansParameters, 10000, "Usual jeans", 2340);
            for (Parameter parameter: jeansParameters)
                parameter.setProduct(jeans);
            jeans = addProduct(jeans);
            Product product1 = productDAO.find(1l);
            Product product5 = productDAO.find(2l);
            Product product7 = productDAO.find(3l);
//        Notification.show(product1.getName());
//        Notification.show(product5.getName());
//        Notification.show(product7.getName());

            List<Product> list = productDAO.findAll();
//        System.out.println(product1);
            //productDAO.removeProduct(product1);

            System.out.println(list.get(0).getName());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Transactional
    public Product getProduct(Long id){
        return productDAO.find(id);
    }

    @Transactional
    public List<Product> getProducts(){
        return productDAO.findAll();
    }

    @Transactional
    public List<ProductField> getProductFields(){
        return productFieldDAO.findAll();
    }

    @Transactional
    public List<ProductType> getProductTypes(){
        return productTypeDAO.findAll();
    }
    @Transactional
    public ProductType getProductType(Long id){
        return productTypeDAO.find(id);
    }

    @Transactional
    public List<Manufacturer> getManufacturers(){
        return manufacturerDAO.findAll();
    }

    @Transactional
    public Product addProduct(Product product){
        return productDAO.save(product);
    }

    @Transactional
    public ProductField addProductField(ProductField productField){
        return productFieldDAO.save(productField);
    }

    @Transactional
    public ProductType addProductType(ProductType productType){
        return productTypeDAO.save(productType);
    }

    @Transactional
    public Manufacturer addManufacturer(Manufacturer manufacturer){
        return manufacturerDAO.save(manufacturer);
    }

    @Transactional
    public void removeProduct(Product product){
        productDAO.remove(product.getId());
    }

    @Transactional
    public void removeProductField(ProductField productField){
        productFieldDAO.remove(productField.getId());
    }

    @Transactional
    public void removeProductType(ProductType productType){
        productTypeDAO.remove(productType.getId());
    }

    @Transactional
    public void removeManufacturer(Manufacturer manufacturer){
        manufacturerDAO.remove(manufacturer.getId());
    }

    @Transactional
    public Product removeParameter(Parameter parameter){
        Product product = parameter.getProduct();
        product.getParameterList().remove(parameter);
        return productDAO.update(product);
    }

    @Transactional
    public List<Product> getProductOfType(ProductType type){
        return productDAO.getProductListOfType(type);
    }

    @Transactional
    public List<ProductType> getProductTypeTree(){
        return productTypeDAO.getTreeOfProductType();
    }

    @Transactional
    public ProductType getProductTypeByName(String name){
        return productTypeDAO.getProductTypeByName(name);
    }

    @Transactional
    public List<Product> getOrderProductList(Order order) {
        return productDAO.getOrderProductList(order);
    }

    @Transactional
    public List<ProductField> getProductFieldsOfType(ProductType productType) {
        return productFieldDAO.getAllProductFieldsOfType(productType);
    }


    @Transactional
    public Product updateProduct(Product product) {
        // JPA not delete orphan parameters automatically
        Product oldProduct = productDAO.find(product.getId());
        for (Parameter parameter: oldProduct.getParameterList()) {
            if (!product.getParameterList().contains(parameter)) {
                parameterDao.remove(parameter.getId());
            }
        }
        return productDAO.update(product);
    }

    @Transactional
    public ProductType updateProductType(ProductType productType) {
        ProductType oldType = productTypeDAO.find(productType.getId());
        if (oldType.getParentType() == null && productType.getParentType() != null) {
            ProductType typeIterator = productType;
            while (typeIterator != null) {
                if (typeIterator.getParentType() != null && typeIterator.getParentType().equals(oldType)) {
                    break;
                }
                typeIterator = typeIterator.getParentType();
            }
            System.out.println("iterator: " + typeIterator);
            System.out.println("old: " + oldType);
            if (typeIterator != null && typeIterator.getParentType() != null && typeIterator.getParentType().equals(oldType)) {
                typeIterator.setParentType(null);
                typeIterator = productTypeDAO.update(typeIterator);
                //typeIterator = productTypeDAO.find(typeIterator.getId());
                productType.getChildTypes().remove(typeIterator);
            }
        }
        return productTypeDAO.update(productType);
    }

}