package ru.ncedu.onlineshop.service.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Али on 13.09.14.
 */
/* TODO смысл существования этого класса мне непонятен. Всё, что тут реализовано, с тем же успехом выполняет сам EntityManager. */
public class GenericDAOImpl<T> implements GenericDAO<T> {

    protected Class<T> entityClass;

    @PersistenceContext
    protected EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public GenericDAOImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass
                .getActualTypeArguments()[0];
    }

    @Override
    public T find(Long id) {
        T object = entityManager.find(entityClass, id);
        return object;
    }

    @Override
    public T save(T t) {
        return entityManager.merge(t);
    }

    @Override
    public T update(T t){
        return entityManager.merge(t);
    }

    @Override
    public void remove(Long id) {
        T managed = entityManager.find(entityClass, id);
        entityManager.remove(managed);
    }

    @Override
    public List<T> findAll(){
        Query query = entityManager.createQuery("from "+ entityClass.getName());
        return query.getResultList();
    }
}
