package ru.ncedu.onlineshop.service.dao.products;

import org.springframework.stereotype.Repository;
import ru.ncedu.onlineshop.entity.product.Parameter;
import ru.ncedu.onlineshop.service.dao.GenericDAOImpl;

@Repository("parameterDao")
public class ParameterDao extends GenericDAOImpl<Parameter> {
    public ParameterDao() {
        super();
    }
}
