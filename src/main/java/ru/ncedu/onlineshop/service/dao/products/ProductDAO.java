package ru.ncedu.onlineshop.service.dao.products;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ncedu.onlineshop.entity.order.Order;
import ru.ncedu.onlineshop.entity.order.OrderItem;
import ru.ncedu.onlineshop.entity.product.Product;
import ru.ncedu.onlineshop.entity.product.ProductType;
import ru.ncedu.onlineshop.service.dao.GenericDAOImpl;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Али on 08.09.14.
 */
@Repository("productDAO")
public class ProductDAO extends GenericDAOImpl<Product> {
    public ProductDAO(){
        super();
    }

    public List<Product> getProductListOfType(ProductType productType){
        TypedQuery<Product> query = entityManager.createQuery("select p from Product p where p.type.id = ?1", entityClass);
        query.setParameter(1, productType.getId());
        return query.getResultList();
    }

//TODO Спросить у Саши на счет перфоманса запроса!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public List<Product> getOrderProductList(Order order){
        // create join contains query
        List<OrderItem> orderItems = order.getOrderItemList();
        TypedQuery<OrderItem> query = entityManager.createQuery("select o from OrderItem o left join fetch o.product where o in ?1", OrderItem.class);
        query.setParameter(1, orderItems);

        //get result of query and create return list
        List<OrderItem> resOrderItemList = query.getResultList();
        List<Product> resProductList = new ArrayList<>(orderItems.size());
        for (OrderItem orderItem: resOrderItemList){
            resProductList.add(orderItem.getProduct());
        }
        return resProductList;
    }

    @Override
    public Product save(Product product){
        Product res = super.save(product);
        entityManager.flush();
        return res;
    }
}