package ru.ncedu.onlineshop.spring;

import com.vaadin.navigator.Navigator;
import org.springframework.stereotype.Component;
import ru.ncedu.onlineshop.entity.order.Order;
import ru.ncedu.onlineshop.entity.users.User;

/**
 * Created by ali on 06.02.15.
 */

@Component
public class WebContext {

    private Navigator navigator;

    public Navigator getNavigator() {
        return navigator;
    }

    public void setNavigator(Navigator navigator) {
        this.navigator = navigator;
    }

}
