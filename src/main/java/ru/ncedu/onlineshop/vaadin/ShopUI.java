package ru.ncedu.onlineshop.vaadin;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import ru.ncedu.onlineshop.service.OrderService;
import ru.ncedu.onlineshop.service.ServiceAPI;
import ru.ncedu.onlineshop.service.UserService;
import ru.ncedu.onlineshop.vaadin.page.AdminPage;
import ru.ncedu.onlineshop.vaadin.page.ManagerPage;
import ru.ncedu.onlineshop.vaadin.page.UserPage;

import javax.servlet.ServletContext;

@Theme("valo")
public class ShopUI extends UI {
    Navigator navigator;
    ApplicationContext context;
    ServiceAPI service;
    OrderService orderService;
    UserService userService;

    protected static final String MAINVIEW = "main";
    {
        ServletContext servletContext = VaadinServlet.getCurrent().getServletContext();
        context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        service = (ServiceAPI) context.getBean("serviceAPI");
        orderService = (OrderService) context.getBean("orderService");
        userService = (UserService) context.getBean("userService");
    }

    @Override
    protected void init(VaadinRequest request) {
//        initializeDatabase();
        initializeNavigator();
        applyBasicPageStyle();
//        Notification.show("start");
    }

    private void initializeDatabase() {
        try {
            service.initializeTestDB();
            userService.initialize();
        } catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    private UserPage userPage;
    private void initializeNavigator() {
        navigator = new Navigator(this, this);
        navigator.addView("", new UserPage(navigator, context));
        navigator.addView("managerPage", new ManagerPage(navigator, context));
        navigator.addView("adminPage", new AdminPage(navigator, context));
    }

    private void applyBasicPageStyle() {
        getPage().setTitle("Shop");
    }
}
