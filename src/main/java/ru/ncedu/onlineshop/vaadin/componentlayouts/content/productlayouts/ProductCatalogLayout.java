package ru.ncedu.onlineshop.vaadin.componentlayouts.content.productlayouts;

import com.vaadin.ui.Button;
import ru.ncedu.onlineshop.entity.order.Order;
import ru.ncedu.onlineshop.entity.product.Product;

/**
 * Created by ali on 29.01.15.
 */
public class ProductCatalogLayout extends InformationalProductLayout {
    private Order order;
    private Button orderProductButton = new Button();
    private boolean wasOrdered = false;

    public ProductCatalogLayout(Order newOrder, final Product product) {
        super(product);
        order = newOrder;
        wasOrdered = order.findProductInOrder(product);
        addOrderProductButtonClickListener(product);
        setOrderProductButtonCaptionDependsOnOrder();
        priceLayout.addComponent(orderProductButton);
    }

    private void addOrderProductButtonClickListener(final Product product) {
        orderProductButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                addOrRemoveProductFromCart();
                setOrderProductButtonCaptionDependsOnOrder();
            }
        });
    }

    private void addOrRemoveProductFromCart() {
        if (wasOrdered == true){
            order.removeProductFromOrder(product);
            wasOrdered = false;
        } else {
            order.addProductToOrder(product);
            wasOrdered = true;
        }
    }

    private void setOrderProductButtonCaptionDependsOnOrder() {
        if (wasOrdered == true){
            orderProductButton.setCaption("Remove from cart");
        } else {
            orderProductButton.setCaption("Add to cart");
        }
    }
}
