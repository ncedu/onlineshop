package ru.ncedu.onlineshop.vaadin.componentlayouts.content.productlayouts;

import com.vaadin.data.Property;
import ru.ncedu.onlineshop.entity.order.OrderItem;
import ru.ncedu.onlineshop.vaadin.componentlayouts.content.parameters.quantity.QuantityLayout;
import ru.ncedu.onlineshop.vaadin.componentlayouts.content.productgrouplyaouts.OrderProductListLayout;

/**
 * Created by ali on 31.01.15.
 */
public class ProductOrderItemLayout extends InformationalProductLayout /*ProductLayout*/{
    private OrderItem orderItem;
    private QuantityLayout quantityLayout;
    //private TextField productQuantity;

    public ProductOrderItemLayout(final OrderItem orderItem, final OrderProductListLayout parentProductList) {
        super(orderItem.getProduct());
        this.orderItem = orderItem;
        quantityLayout = new QuantityLayout(orderItem.getQuantity());
        addProductQuantityValueChangeListener(parentProductList);
        addComponent(quantityLayout);
    }

    private void addProductQuantityValueChangeListener(final OrderProductListLayout parentProductList) {
        quantityLayout.getQuantityTextEdit().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                try {
                    int quantity = Integer.parseInt(quantityLayout.getQuantityTextEdit().getValue());
                    orderItem.setQuantity(quantity);
                    parentProductList.changeTotalPrice();
                } catch (NumberFormatException exception) {
                    // TODO: log it!
                }
            }
        });
    }
}
